FROM centos:7.4.1708

USER root

COPY jdk-8u231-linux-x64.rpm .
RUN rpm -ivh jdk-8u231-linux-x64.rpm

RUN useradd -m -u 1000 maven

RUN curl -sSfkLO https://www-us.apache.org/dist/maven/maven-3/3.6.2/binaries/apache-maven-3.6.2-bin.tar.gz

RUN tar xvf apache-maven-3.6.2-bin.tar.gz
RUN mv apache-maven-3.6.2 /opt
RUN ln -s /opt/apache-maven-3.6.2 /opt/maven

RUN mkdir /var/maven_workspace
RUN chown 1000:1000 /var/maven_workspace

USER maven

ENV JAVA_HOME=/usr/java/jdk1.8.0_231-amd64
ENV PATH=$PATH:/opt/maven/bin



## Oracle JDK入りMavenコンテナ


Dockerイメージ作成
```
docker build --no-cache -t maven_oracle_jdk .
```

実行
```
docker run --rm -v /var/build_workspace/some_project:/var/maven_workspace -w /var/maven_workspace maven_oracle_jdk
````

## 参考ページ
https://qiita.com/tanan/items/e79a5dc1b54ca830ac21
https://www.project-respite.com/docker-maven/
https://hub.docker.com/_/maven/
